function showAlert(message, type, time){
    if(time==undefined) time=3000;
    if(type==undefined) type='info';
    var alert = '<div style="z-index:5000;position:fixed;top:80px;left:55%;transform: translateX(-50%);" class="alert alert-'+type+' alert-dismissible fade show" role="alert"><button type="button" class="close" aria-label="Close" data-dismiss="alert"><span aria-hidden="true">×</span></button>'+message+'</div>';
    $('body').append(alert);
    setTimeout(function(){
        $('.alert .close').click();
    },time);

}

window.addEventListener("resize", function(){
    if(window.innerWidth < 992){
        $('.card-menu .heading-left a.text-logo').hide();
        $('.card-menu .heading-left a.ico-logo').show();
    } else {
        $('.card-menu .heading-left a.text-logo').show();
        $('.card-menu .heading-left a.ico-logo').hide();
    }
    if(document.body.clientWidth<1250){
        $(".app-container").addClass("closed-sidebar-mobile closed-sidebar");
    }else{
        $(".app-container").removeClass("closed-sidebar-mobile closed-sidebar");
    }
});
window.addEventListener('load', function() {
    page();
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        // if (form.checkValidity() === false) {
        //   event.preventDefault();
        //   event.stopPropagation();
        // }
        event.preventDefault();
        event.stopPropagation();
        form.classList.add('was-validated');
      }, false);
    });
    
    // $('.date, .datetime').attr('autocomplete', 'off');
    var dt = document.getElementsByClassName('date');
    var i;
    for (i = 0; i < dt.length; i++) {
        dt[i].setAttribute('autocomplete','off');
    } 
    var dtime = document.getElementsByClassName('datetime');
    for (i = 0; i < dtime.length; i++) {
        dtime[i].setAttribute('autocomplete','off');
    } 
}, false);

var uiDatetimeFormat = 'DD.MM.YYYY HH:mm';
var dbDatetimeFormat = 'YYYY-MM-DD HH:mm:ss';
var uiDateFormat = 'DD.MM.YYYY';
var dbDateFormat = 'YYYY-MM-DD';

function page(){
    var path = window.location.pathname;
    var page = path.split("/").pop();
    onLoadPage(page);
}

function parseFormData(formId){
    let formData = new FormData($('#'+formId)[0]);

    //iterating form data
    for(var pair of formData.entries()) {
        //date
        if($('#'+formId+' input[name="'+pair[0]+'"]').hasClass('date')){
            formData.set(pair[0], moment(pair[1], uiDateFormat).format(dbDateFormat));
        }
        //datetime
        if($('#'+formId+' input[name="'+pair[0]+'"]').hasClass('datetime')){
            formData.set(pair[0], moment(pair[1], uiDatetimeFormat).format(dbDatetimeFormat));
        }
        //money
        if($('#'+formId+' input[name="'+pair[0]+'"]').hasClass('money')){
            formData.set(pair[0], accounting.unformat(pair[1]));
        }
    }
    return formData;
}
function parseInputData(key,value,storage){
    let element = $('#'+key);
    storage = typeof(storage)=='undefined' ? '' : storage;
    if(element.attr('type')==='file'){
        element.attr('required',false);
        $('#'+key+'_filename').text(value).attr('href',storage+"/"+value);

    }else if(element.hasClass('money')){
        element.val(Number(value));

    }else if(element.hasClass('date')){
        element.val(moment(value, dbDateFormat).format(uiDateFormat));

    }else if(element.hasClass('datetime')){
        element.val(moment(value, dbDatetimeFormat).format(uiDatetimeFormat));
    }else if(typeof(value)=='boolean'){
        element.val(value?'1':'0');
    }else{
        element.val(value);
    }
    // console.log(key,value);
}

function defaultAjaxFail(jqXHR, textStatus, errorThrown){
    let message = "Data not saved.";
    try{
        message = jqXHR.responseJSON.message;
    }catch(e){}
    showAlert(message, "danger", 3000);
}


var auth = {
    login : function($value){
        sessionStorage.setItem("uid", $value);
        if ($value=="vendor"){
            window.location.href = "vendor_profile.html";
        }else{
            window.location.href = "dashboards.html";
            event.preventDefault();
        }
    },
    logout :function(){
        sessionStorage.clear();
        window.location.href = "../index1.html";
    },
    get :function(){
        return sessionStorage.getItem("uid");
    }
}

var auth2 = {
    login : function($value){
        sessionStorage.setItem("uid", $value);
        if ($value=="vendor"){
            window.location.href = "poc-eproc/vendor_profile.html";
        }else{
            window.location.href = "poc-eproc/home.html";
            event.preventDefault();
        }
    },
    logout :function(){
        sessionStorage.clear();
        window.location.href = "../index1.html";
    },
    get :function(){
        return sessionStorage.getItem("uid");
    }
}

var Loading = {
    Show : function(selector){
        if(selector){
            $(selector).LoadingOverlay("show", {
                image       : "",
                fontawesome : "fa fa-spinner fa-spin",
                fontawesomeColor : '#52BA92'
            });
        }else{
            $.LoadingOverlay("show", {
                image       : "",
                fontawesome : "fa fa-spinner fa-spin",
                fontawesomeColor : '#52BA92'
            });
        }
    },
    Hide : function(selector){
        if(selector){
            $(selector).LoadingOverlay("hide");
        }else{
            $.LoadingOverlay("hide");
        }
    }
}

function onLoadPage(page){
    console.log( page );
    if (auth.get()=="vendor"){
        if (page=="dashboards.html"){
            window.location.href="applicants_profile.html";
        }else if (page=="vendors_profile_37.html"){
            window.location.href="vendor_edit-profile_general.html";
        }
        //hideVendorMenu();
    }
    $("#navbarDropdownUser strong").text(auth.get());
    console.log( auth.get() );
}

function hideVendorMenu(){
    var x = document.querySelector("a").getAttribute;
    console.log(x);
      
}
